#coding:utf-8
import config
import os

def announce_change(id) :
    with open(config.TORRENT_DIR + id + '.torrent','rb') as f:
        torrent_ct = f.read()
    tracker = config.MY_TRACKER
    pos = torrent_ct.find('announce')
    leng = int(torrent_ct[pos + 8:torrent_ct.find(':', pos)])
    mod = '%s%d:%s%s' % (torrent_ct[:pos+8], len(tracker), tracker, torrent_ct[pos+11+leng:])
    os.remove(config.TORRENT_DIR + id + '.torrent')
    with open(config.TORRENT_DIR + id + '.torrent','wb') as f:
        f.write(mod)