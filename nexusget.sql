-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 10 月 06 日 12:13
-- 服务器版本: 5.5.14
-- PHP 版本: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `nexusget`
--

-- --------------------------------------------------------

--
-- 表的结构 `torrent`
--

CREATE TABLE IF NOT EXISTS `torrent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `torrent_info_hash` varchar(40) NOT NULL,
  `ori_torrent_id` int(11) NOT NULL,
  `source_url` text,
  `title` text CHARACTER SET utf8,
  `main` text CHARACTER SET utf8,
  `link` text CHARACTER SET utf8,
  `status` tinyint(4) NOT NULL DEFAULT '-1',
  `gift_code` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oid` (`ori_torrent_id`),
  KEY `torrent_info_hash` (`torrent_info_hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=228 ;

--
-- 转存表中的数据 `torrent`
--

INSERT INTO `torrent` (`id`, `torrent_info_hash`, `ori_torrent_id`, `source_url`, `title`, `main`, `link`, `status`, `gift_code`) VALUES
(27, '0b77985699f34fa2dacfa6ce0662fe0624c4fb14', 169659, NULL, 'Jeepers Creepers II 2003 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbaw2c9ncc'),
(29, '7dfcf8f9676cdc3319f1d8cb7ebff729e5976e1b', 169627, NULL, 'Much Ado About Nothing 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbaw2cytax'),
(36, '7db181fef5a6edb4599a54f917041582e2ec02f4', 169518, NULL, 'Before Midnight 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbdbsk9psd'),
(40, '055a7d8f97cc68c9d32d56b8375411dccc025c7e', 169402, NULL, 'Much Ado About Nothing 2012 720p DTS x264-CHD', NULL, NULL, -1, '5lbaw2c73db'),
(42, '5d608a5936d1eb724595b789897a5a9be22e309c', 169277, NULL, 'Shield of Straw 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbdbsk076y'),
(48, '34b5ac8a219cc4f75a6a9a74f45d7f00f86ba42c', 169698, NULL, 'The Haunting 1963 720p BluRay X264-AMIABLE', NULL, NULL, -1, '5lbaw2cyowt'),
(49, 'c7c24ea4dd28be1728ed39b578f9efce1421572c', 169691, NULL, 'In the Mouth of Madness 1994 720p BluRay X264-AMIABLE', NULL, NULL, -1, '5lbe4290sjw'),
(91, '77d3d10f4bc46e359958fb8e796d8127a86fcf14', 169650, NULL, 'Sergeant York 1941 720p WEB-DL H264-GABE', NULL, NULL, -1, '5lbb9boweqj'),
(106, '9bfb27122550b8e4deb653affcae1beefb3e3e5e', 169550, NULL, 'The Heat 2013 UNRATED BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbaw2lskdi'),
(113, '86192184c0eadd36a361347c5c0b8c59315b68e2', 169434, NULL, 'The Lone Ranger 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(114, '6d68fdcc4cce10b3bb973b974d03deaaa23990c9', 169474, NULL, 'The Lone Ranger 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(115, 'dfd2eedbbae10b26006f40ef2cb0dfd2b3ef5110', 168443, NULL, 'Europa Report 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(116, '25812f57604bf97147e30ff1fa227da3f922b62c', 169552, NULL, 'The Heat 2013 UNRATED BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(117, '047dd307ebe4d7b1256b840f2653c66f3de40f74', 169535, NULL, 'Race 2 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(118, '2cc1af4b9a972a9d45be8837c23ee57197116b74', 169632, NULL, 'Before Midnight 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(119, '05cd44e47d4e700d6374b6074672f9776403e636', 169662, NULL, 'Pineapple Express 2008 REMASTERED BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(120, 'eaa443ec11c32ebba95728fc400c284121bcfacc', 169728, NULL, 'Jeepers Creepers II 2003 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(121, '1e9e079c0bba215acee1db8ba16002d7eb089912', 88420, NULL, 'The Flowers of War 2011 HD 720p DD2.0 x264-Jobs', NULL, NULL, -1, ''),
(122, '', 76342, NULL, 'Transformers Dark Of The Moon 2011 Repack BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(123, '', 137221, NULL, 'CZ12 2012 720p WEB-DL x264 AC3', NULL, NULL, -1, ''),
(124, '', 146962, NULL, 'Jack Reacher 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(125, '', 85570, NULL, '10 Years Love 2008 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(126, '', 116203, NULL, 'The Avengers 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(127, '', 113153, NULL, 'Cabaret Desire 2011 720p BluRay DTS x264-CHD', NULL, NULL, -1, ''),
(128, '52b23580783df9f2a24da67c17e0b8229471e492', 105284, NULL, 'Act of Valor 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(129, '74ad400e73905e7c16dbeba211104fab8e1fb40b', 121151, NULL, 'The Expendables 2 2012 BluRay 720p AC3 x264-CHD', NULL, NULL, -1, ''),
(130, '7dede14a0282dda083c02b03a647158b0b8f2ba3', 90890, NULL, 'The 33d Invader 2011 BluRay 720p DTS 2Audio x264-CHD', NULL, NULL, -1, ''),
(131, '9a5cb20fca6edd0e8209d62c718a8c9c412ce7cc', 137753, NULL, 'My PS Partner 2012 HDTV 720p AC3 x264-CHD', NULL, NULL, -1, '5lbc8w6rsee'),
(132, '6d7d0c10d459bd883ca8f37f5182fc4905d7e9c7', 99637, NULL, 'Mission Impossible-Ghost Protocol 2011 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(133, '57e13006ebda0b1ab86112290c3be3a1460294bd', 156859, NULL, 'American Dreams in China 2013 720p WEB-DL x264 AC3', NULL, NULL, -1, ''),
(134, 'd81b15037f9dfbd00c44d98fa0c5975063a48173', 104998, NULL, 'Guns and Roses 2012 WEB-DL 720p x264 AC3-DWR', NULL, NULL, -1, '5lbb9bbeiqj'),
(135, 'eb9b6bc2a0d8a167015a0cff43f123e3970a161f', 81393, NULL, 'Rise of the Planet of the Apes 2011 BluRay REPACK 1080p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbb9azz5ca'),
(136, '16fdec9ee9b76d538a3ee64574157adee32686cd', 149696, NULL, 'Finding Mr Right 2013 HDTV 720p x264 AAC', NULL, NULL, -1, '5lbaw2r053f'),
(137, '9b92ee2587eb6b57a37ce79112396a21bedb17e3', 165119, NULL, 'World War Z 2013 UNRATED BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbc8w68y7k'),
(138, 'fe10f95d9c02a34f491fd94d72eb0f617fce68b5', 148753, NULL, 'Parker 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbc8ww9soh'),
(139, 'ff5a0c6f096a71ba4fc8f4495eb23e23202a47d8', 81044, NULL, 'Rise of the Planet of the Apes 2011 BluRay 720p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbc8wwjk29'),
(140, 'f52a9f6f20f162a407e90f15a36adc65508c2724', 125956, NULL, 'Lan Kwai Fong 2 2012 BluRay 720p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbdbssp43d'),
(141, '31f5ce793173879450cfa2fb7e579b47eb8ef963', 165095, NULL, 'World War Z UNRATED CUT 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbc8w6k6rt'),
(142, '2b4f352575134ee71aa87b6be8815fd4ae6336e3', 139512, NULL, 'The Hobbit: An Unexpected Journey 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbaw2redvi'),
(143, 'a4c8f38e477cfc0d16e43996db920f9b903859b8', 128347, NULL, 'Vulgaria 2012 BluRay 720p AC3 2Audio x264-CHD', NULL, NULL, -1, '5lbaw2zcw5y'),
(144, '81359fd281194773b60b72bbdeedcfa4d5efceaf', 129822, NULL, 'Cold War 2012 HDRip 720p AC3 x264', NULL, NULL, -1, '5lbdbsuuo48'),
(145, '834e6f69c95a56dc351f889640e7c8247b18d55f', 123550, NULL, 'The Amazing Spider-Man 2012 BluRay 1080p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbaw2budj3'),
(146, '23f42e55dc507cd187bf922c5faf206b2befe808', 125163, NULL, 'Total Recall 2012 EXTENDED DC BluRay 1080p AC3 x264-CHD', NULL, NULL, -1, '5lbb9biugx6'),
(147, '1be1f562f896e116d577429d0cb059f10dad0988', 86508, NULL, 'Moneyball 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbaw2kduq3'),
(148, '157aa3d1bb89030a0e696c3f52c8cd5fb9f0b4a8', 151380, NULL, 'A Good Day to Die Hard extended 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbb9bi9gea'),
(149, '393450b8379dfac5621c886f8b466e123e34c81d', 125062, NULL, 'Ted 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbc8wwmdbi'),
(150, '89ac6a6bf1e8d64abb6fabc97c316b902ffadf66', 89014, NULL, 'Real Steel 2011 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbdbs6iv8y'),
(151, '1c1b051b5048d241b8410805fe9e18f9908a08fc', 158051, NULL, 'G I Joe Conspiration 2013 Extended Action Cut BluRay 720p AC3 x264-CHD', NULL, NULL, -1, '5lbaw2bvpme'),
(152, '6d85e937546ec7f8531969d08ccb68f68ca67cbc', 106196, NULL, 'Safe House 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbb9b54ri6'),
(153, 'f65f8be12af67bc5e6c10bac18d1e2fc1209356e', 147882, NULL, 'Journey To The West Conquering The Demons 2013 720p HDTV x264 AC3', NULL, NULL, -1, '5lbaw22z5ic'),
(154, '332c2c5efeec48bc03799a14ef69f7707f1f139f', 127404, NULL, 'Fifty Shades Of Grey 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe42uu2tu'),
(155, '8250093664b9b1da0a06bab7b26325f320021c7d', 72663, NULL, 'X-Men First Class 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe42kotaw'),
(156, '5f4667219a0f13915e9337d7dd44d010b98ec060', 153354, NULL, 'My Awkward Sexual Adventure 2012 BluRay 720p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbc8wwu6gk'),
(157, '185364eabc9a5708586ca80aa5bee076fb3adf7a', 105338, NULL, 'Act of Valor 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbdbs0h49a'),
(158, '8f9cbb022f930f9afe78c7052e289cb40e1e3911', 152690, NULL, 'Jack The Giant Slayer 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbdbsct1e6'),
(159, 'f3bc81907742616f3b1741735ed5a086616da22a', 156318, NULL, 'Olympus Has Fallen 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe42u2mqj'),
(160, 'd187b6b25daec58dcbcd69c76bccc62a8fc433d9', 159562, NULL, 'Oblivion 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbaw2bspif'),
(161, '2f03ea98737e3a09d4d610885f9b8e34ce21e955', 169758, NULL, 'Curse Of Chucky 2013 Unrated BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbc8w7uvo1'),
(162, '7edf41e96b8e40c098e10d1c94ed19b35f87dee2', 126195, NULL, 'The Dark Knight Rises 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbb9bfsgca'),
(163, '', 136256, NULL, 'Skyfall 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(164, 'fd9e3f48d7ad425773def464b0785a2f4cdc66e5', 143483, NULL, 'Django Unchained 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbaw275y2b'),
(165, '3dc7457462c16c0fb337ce4cc58329c7a72f86f2', 99616, NULL, 'Mission Impossible Ghost Protocol 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbaw26f0cc'),
(166, 'cf65c14c2174a65219086db8b7218ff02319f691', 163377, NULL, 'Iron Man 3 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbc8wnvwrk'),
(167, '6954516dae5bb2d9230a918d37001d52ccb40221', 125157, NULL, 'Total Recall 2012 EXTENDED DC BluRay 720p AC3 x264-CHD', NULL, NULL, -1, '5lbc8wsybwv'),
(168, '45bd60f01f62c60c7a25b95279e70817246c70ff', 68567, NULL, 'Source Code 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbb9bfdwkm'),
(169, 'a0adcd2c8553edce8f1ee0837aa8d3e0f53d7b82', 88918, NULL, 'Real Steel 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbb9bfj6i6'),
(170, 'b639258338d34f482cbb54fc42c891dec1d387fa', 156343, NULL, 'Olympus Has Fallen 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbb9bfcgas'),
(171, '', 65133, NULL, 'TLOTR Trilogy Extended BluRay 1080p DTSES6 1 2Audio x264-CHD', NULL, NULL, -1, ''),
(172, '508f3b70648437a72f083cea769eaeee5f338201', 94629, NULL, 'Hugo 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe4u1dylq'),
(173, '7fd2ca4b19aa0ed71ab33add87e81a6c75eaa707', 72877, NULL, 'Thor 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe425w8oc'),
(174, 'afc05da8b0ef3ad3578d0b2656dfd43275d3fc93', 164199, NULL, 'Star Trek Into Darkness 2013 BluRay 1080p AC3 x264-CHD', NULL, NULL, -1, '5lbb9bija1s'),
(175, '8565c148a16f7d1a62234edf3f0ed7aa14fb57e3', 120271, NULL, 'PROMETHEUS 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbdbsgjyow'),
(176, '2e54d61db77dd829062c1d8d08955ae27f41f2ff', 130197, NULL, 'Tai Chi Hero 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbb9bfduok'),
(177, '49b1c18aa80cb277763f07cf40ee06a678780a90', 124042, NULL, 'Double Xposur 2012 720p WEB-DL x264 AC3', NULL, NULL, -1, '5lbe42m330a'),
(178, 'afd16412fb7358329f69fdca1c2710cc8030f357', 151638, NULL, 'Oz The Great and Powerful 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbb9bqjafs'),
(179, 'd92cd9dd360808e738012458854062d03d7a7493', 85068, NULL, 'The Thing 2011 Repack BluRay 720P DTS x264-CHD', NULL, NULL, -1, '5lbawb8ggre'),
(180, '621d71420224847a0adfcdbb69974cfb3719d70a', 121793, NULL, 'The Bullet Vanishes 2012 HDTV 720p x264 AC3', NULL, NULL, -1, '5lbaw2axg1e'),
(181, '2763b18016458348e457163d3bee07a917459c7c', 140524, NULL, 'The Last Tycoon 2012 BluRay 720p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbc8wo5vem'),
(182, '89be77596af14cb720567a58aed596063294cc68', 72178, NULL, 'Fast Five 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbawb81f43'),
(183, '3f2c003e4341b622ee88ae8530b55ffdb2b5cb95', 105567, NULL, 'John Carter 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbc8vxz84h'),
(184, 'b275651ff18bb85b9d294a2eaec20b02ab4af2e6', 139481, NULL, 'The Hobbit: An Unexpected Journey 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbc8wospzk'),
(185, '26c3fe978bc72a14602f2e22c942945923336cb1', 106735, NULL, 'Iron Sky 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbawb13h0b'),
(186, 'b496c5b9a41d8464014527c34d62e39d5d29952f', 159611, NULL, 'Oblivion 2013 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbe4uciorc'),
(187, 'b4a9e1a4846d0b1a6dea2dfa6c3c587a4f28f23c', 163332, NULL, 'Iron Man 3 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbdb1jkgnw'),
(188, '44e433817c95d34a658f2548160fde88e607140b', 158092, NULL, 'G I Joe Conspiration 2013 Extended Action Cut BluRay 1080p AC3 x264-CHD', NULL, NULL, -1, '5lbc8vycz41'),
(189, '3b7eb7b40df1f294821e3c92c05224b74e8a0139', 109860, NULL, 'American Reunion 2012 UNRATED BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe4uac6fa'),
(190, 'c611dc870ecddd4514464ae142f9dc1b781a7122', 134968, NULL, 'Flight 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe4u1dbdu'),
(191, '4bd12ced19c41f78070573a883292505c3d255e9', 110070, NULL, 'Lockout 2012 UNRATED BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe4ucfv6j'),
(192, '18af7b0fa1cdf4150e2a7f75324e68ca7e9115c9', 124207, NULL, 'The Intimate 2005 BluRay 720p AC3 x264-CHD', NULL, NULL, -1, '5lbdbsgf5vd'),
(193, 'f676988f6d26c1781665b6f5b9aecc50628f2a00', 128423, NULL, 'Resident Evil Retribution 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbe4urljmw'),
(194, '9ae58634703d4ee6c79d9d17ab2bdecdaa9ac525', 129801, NULL, 'Cloud Atlas 2012 BluRay 720p AC3 x264-CHD', NULL, NULL, -1, '5lbb9ak2nb6'),
(195, '7fa9fdef26d6a147f96d7c218f5b441f74db7bf4', 72560, NULL, 'Pirates of the Caribbean On Stranger Tides 2011 BluRay 720p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbb9apy3b6'),
(196, 'ca97e6fce56d6ca2b31a4dce10239bbfb4fb590c', 133488, NULL, 'Pirates of the Caribbean On Stranger Tides 2011 BluRay 720p DTS 2Audio x264-CHD', NULL, NULL, -1, '5lbe4u0wj6j'),
(197, '395b955eb4c9182177afaeee841c9bf07aa1cb23', 108280, NULL, 'The Thieves 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbc8vy9ha9'),
(198, 'bda5c28813bcb787c203d8c6c9ddce08273ee07b', 151559, NULL, 'Wrath Of The Titans 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbe4u0by2q'),
(199, '0186d8c00ac5a16df9450e0829711dd0be2c8917', 169767, NULL, 'Fright Night 2 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(200, '6ab5d620f044d2c3f799aed0a26a427611000e56', 140756, NULL, 'Say Yes 2013 720p HDTV x264 AC3', NULL, NULL, -1, '5lbawb15hlt'),
(201, 'ca544ceeb5085a90a17415d87bf257ceeae109eb', 114079, NULL, 'The Hunger Games 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbe4ue9kiw'),
(202, '46f3d3e89f0589735d8e151020eb7b8a1a27aed4', 72985, NULL, 'Thor 2011 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(203, '53e9c7d0571f04f196288db48c6a3b99c4185e4d', 105456, NULL, 'John Carter 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbawbvysat'),
(204, '92b20f4231b008ad43372d23923449cb7dd5cb4b', 108254, NULL, 'Wrath Of The Titans 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbb9agmtfm'),
(205, '334a4b152a71ccc1b349822c774377591aed32be', 143552, NULL, 'Django Unchained 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbc8vfzgq1'),
(206, 'b64c37887bd9b6c37b564b18c16ea47f04d58f29', 84419, NULL, 'Catch 44 2011 BluRay 1080p AC3 x264-CHD', NULL, NULL, -1, '5lbdb1l2oky'),
(207, '70334dc79289c12cef356811a4bf0fa3a17cf835', 114366, NULL, 'Caught in the Web 2012 HDTV 720p x264 AC3', NULL, NULL, -1, '5lbc8vjn4te'),
(208, '7593312c7e575241545063eb9737705fa2c18c65', 127637, NULL, 'The Bourne Legacy 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbb9apm304'),
(209, '1fee4ed90eb0e707f9a4876acc46f6b16b27970c', 103353, NULL, 'The Grey 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(210, '2f5e0db9d545a222a9d8e0d87be87a80db0df2b9', 118204, NULL, 'Men in Black 3 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbe4uwcpcw'),
(211, 'f75815c4eeb32ee22cfabb355d6f862e3c242a71', 113025, NULL, 'Under The Temptation 2012 HDTV 720p x264 AC3', NULL, NULL, -1, '5lbe4ugpgpc'),
(212, '69596feafef2782974d0ab1ffbe52dbc51ee7f60', 78325, NULL, 'Captain America：The First Avenger 2011 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(213, '1965ec3e060df454efcc3e5a2caa7a342963fb35', 152693, NULL, 'Jack The Giant Slayer 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbawbf8rtc'),
(214, '45ae1185fb1f01a392fdb445b3579080b7b28778', 111814, NULL, 'Black White Episode 1 The Dawn of Assault 2012 HDTV 720p x264 AC3', NULL, NULL, -1, '5lbe4uwui3m'),
(215, '6570122d9efa2dafe081d3ace4947da05fd1d80d', 130890, NULL, 'Dredd 2012 BluRay 1080p DTS x264-CHD', NULL, NULL, -1, '5lbb9a27fe6'),
(216, '87da40d3fecff7ad19e27e02fcb4bb90c9a66d98', 86575, NULL, 'The Ides of March 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(217, '7c121bc7f8949f2c162e908b56562ebb2300f03a', 78255, NULL, 'Captain America The First Avenger 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '5lbdb1i2xnw'),
(218, 'd884edb20e91ea0db4e69484d2047f6e2d712aa7', 150111, NULL, 'Hansel and Gretel Witch Hunters 2013 Unrated Cut BluRay 720p AC3 x264-CHD', NULL, NULL, -1, '5lbe4usi0za'),
(219, '4285ee5caba7a8938bc2b37d1475b262f1a2bad7', 131718, NULL, 'Taken 2 2012 UNRATED EXTENDED CUT BluRay 1080p DTS x264-CHD', NULL, NULL, -1, ''),
(220, '701fb5fc9f93c7972db3816b43b42ed3c8f4c9ef', 147600, NULL, 'The Grandmaster 2013 BluRay 1080p DTS 2Audio x264-CHD', NULL, NULL, -1, ''),
(221, '3217f22b5a378e7f0321f0974a523ce55765ed9f', 103139, NULL, 'Red Tails 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(222, '290a1478c3ccea42a4cb764129097c185f1e1b96', 103872, NULL, 'This Means War 2012 UNRATED BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(223, '2913160e5f92b49295ec8296a72c8fb3712e0245', 126159, NULL, 'The Dark Knight Rises 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(224, '9346a4c221c7692b8ef5bb788597436903abf342', 119937, NULL, 'Meet The In Laws HDTV 720p x264 AC3', NULL, NULL, -1, '5lbdb1ism8y'),
(225, 'f8b212ee7c2ca943a99c74383452f01bafc607ad', 65466, NULL, 'The Adjustment Bureau 2011 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(226, '832762f7fb0adcce006941fe41cb5157be61c4db', 139476, NULL, 'Life of Pi 2012 BluRay 720p DTS x264-CHD', NULL, NULL, -1, ''),
(227, 'cf89a22363f3ca73118a3c72a0f437ca3886c506', 169780, NULL, 'Pacific Rim 2013 BluRay 720p DTS x264-CHD', NULL, NULL, -1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
